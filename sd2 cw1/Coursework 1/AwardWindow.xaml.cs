﻿/* Author Name: Nathan Mair
*  Description: Interaction logic for AwardWindow.xaml
*  Date last modified: 19/10/2015
*/
using System.Windows;

namespace Coursework_1
{
    public partial class AwardWindow : Window
    {
        public AwardWindow()
        {
            InitializeComponent();
        }

        //Closes the window when close button is clicked
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
