﻿/* Author Name: Nathan Mair
*  Description: Definition of the ResearchStudent class
*  Date last modified: 22/10/2015
*/

using System;

namespace Coursework_1
{
    public class ResearchStudent : Student
    { 
        //Initialising variables
        private string course = "PhD";
        private string supervisor = "Prof. E Ellington";
        private string topic = "Rear-end and elbow differentiation in the 21st century.";
        private bool isChecked = false;

        public string Supervisor
        {
            //Returns current value of supervisor when it is requested
            get
            {
                return supervisor;
            }

            //Sets supervisor to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if supervisor field is empty
                    throw new ArgumentException("Supervisor field is empty.");
                }
                supervisor = value;
            }
        }

        public string Topic
        {
            //Returns current value of topic when it is requested
            get
            {
                return topic;
            }

            //Sets topic to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if topic field is empty
                    throw new ArgumentException("Topic field is empty.");
                }
                topic = value; 
            }
        }

        //Changes the value of isChecked when it is changed in MainWindow
        public void Checked(bool checkedFromWindow)
        {
            isChecked = checkedFromWindow;
        }

        //Opens the ResearchAwardWindow with all award information
        new public void award()
        {
            //Checks that research student is level 4
            if (Level == 4)
            {
                ResearchAwardWindow a = new ResearchAwardWindow();
                a.lblMatricNumber.Content = (MatriculationNumber);
                a.lblName.Content = (SecondName + ", " + FirstName);
                a.lblTopicValue.Content = (topic);
                a.lblAwardValue.Content = ("Doctorate of Philosophy");
                a.Show();
            }
            else
            {
                //Throws exception if research student is not level 4
                throw new ArgumentException("Research student must be level 4 to be awarded.");
            }
        }
    }
}
