﻿/* Author Name: Nathan Mair
*  Description: Interaction logic for MainWindow.xaml
*  Date last modified: 22/10/2015
*/

using System;
using System.Windows;

namespace Coursework_1
{
    public partial class MainWindow
    {
        public Student s = new Student();
        public ResearchStudent rs = new ResearchStudent();

        public MainWindow()
        {
            InitializeComponent();
        }

        //Attempts to set all the values in the student class to the values in each text box
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            if (chkResearchStudent.IsChecked == false)
            {
                //For normal student
                try
                {
                    s.MatriculationNumber = Convert.ToInt32(txtMatricNumber.Text);
                    s.FirstName = txtFirstName.Text;
                    s.SecondName = txtSecondName.Text;
                    s.DateOfBirth = txtDateOfBirth.Text;
                    s.Course = txtCourse.Text;
                    s.Level = Convert.ToInt32(txtLevel.Text);
                    s.Credits = Convert.ToInt32(txtCredits.Text);
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
            else
            {
                //For research student
                try
                {
                    rs.MatriculationNumber = Convert.ToInt32(txtMatricNumber.Text);
                    rs.FirstName = txtFirstName.Text;
                    rs.SecondName = txtSecondName.Text;
                    rs.DateOfBirth = txtDateOfBirth.Text;
                    rs.Course = "PhD";
                    rs.Level = 4;
                    rs.Credits = Convert.ToInt32(txtCredits.Text);
                    rs.Supervisor = txtSupervisor.Text;
                    rs.Topic = txtTopic.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Clears all the text boxes
            txtMatricNumber.Clear();
            txtFirstName.Clear();
            txtSecondName.Clear();
            txtDateOfBirth.Clear();
            txtLevel.Clear();
            txtCredits.Clear();
            txtSupervisor.Clear();
            txtTopic.Clear();
            //Doesn't clear course if chkResearchStudent is checked as it is always PhD
            if (chkResearchStudent.IsChecked == false)
            {
                txtCourse.Clear();
            }
            }

        //Gets all the values from either the Student class or the ResearchStudent class depending on whether chkResearchStudent is checked
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            if (chkResearchStudent.IsChecked == false)
            {
                //Fills the text boxes with the values currently held in the Student class
                txtMatricNumber.Text = s.MatriculationNumber.ToString();
                txtFirstName.Text = s.FirstName;
                txtSecondName.Text = s.SecondName;
                txtDateOfBirth.Text = s.DateOfBirth;
                txtCourse.Text = s.Course;
                txtLevel.Text = s.Level.ToString();
                txtCredits.Text = s.Credits.ToString();
            }

            else
            {
                //Fills the text boxes with the values currently held in the ResearchStudent class
                txtMatricNumber.Text = rs.MatriculationNumber.ToString();
                txtFirstName.Text = rs.FirstName;
                txtSecondName.Text = rs.SecondName;
                txtDateOfBirth.Text = rs.DateOfBirth;
                txtCredits.Text = rs.Credits.ToString();
                txtLevel.Text = rs.Level.ToString();
                txtSupervisor.Text = rs.Supervisor.ToString();
                txtTopic.Text = rs.Topic.ToString();
            }

        }

        //Calls the award method from either the Student class or the ResearchStudent class depending on whether chkResearchStudent is checked 
        private void btnAward_Click(object sender, RoutedEventArgs e)
        {
            if (chkResearchStudent.IsChecked == false)
            {
                //Calls the award method in the Student class
                s.award();
            }
            else
            {
                //Calls the award method in the ResearchStudent class
                try
                {
                    rs.award();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
            }
        }

        //Calls the advance method in the Student class
        private void btnAdvance_Click(object sender, RoutedEventArgs e)
        {
                try
                {
                    s.advance();
                    txtLevel.Text = s.Level.ToString();
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                }
        }

        //Tells ResearchStudent class that chkResearchStudent is checked and enables/disables fields where appropriate
        private void chkResearchStudent_Checked(object sender, RoutedEventArgs e)
        {
            txtCourse.Text = "PhD";
            txtCourse.IsEnabled = false;
            txtTopic.IsEnabled = true;
            txtSupervisor.IsEnabled = true;
            rs.Checked(true);
        }

        //Tells ResearchStudent class that chkResearchStudent is unchecked and enables/disables fields where appropriate
        private void chkResearchStudent_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCourse.Text = "";
            txtCourse.IsEnabled = true;
            txtTopic.Text = "";
            txtTopic.IsEnabled = false;
            txtSupervisor.Text = "";
            txtSupervisor.IsEnabled = false;
            rs.Checked(false);
        }
    }




}
