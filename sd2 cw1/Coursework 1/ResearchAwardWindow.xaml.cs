﻿/* Author Name: Nathan Mair
*  Description: Interaction logic for ResearchAwardWindow.xaml
*  Date last modified: 22/10/2015
*/
using System.Windows;

namespace Coursework_1
{

    public partial class ResearchAwardWindow : Window
    {
        public ResearchAwardWindow()
        {
            InitializeComponent();
        }

        //Closes the window when close button is clicked
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
