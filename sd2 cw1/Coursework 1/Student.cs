﻿/* Author Name: Nathan Mair
*  Description: Definition of the student class
*  Date last modified: 22/10/2015
*/

using System;

namespace Coursework_1
{
    public class Student
    {
        //Initialising variables
        //When set these properties hold the values in each field in the MainWindow
        private int matriculationNumber = 40170;
        private string firstName = "Nathan";
        private string secondName = "Mair";
        private string dateOfBirth = "14/10/1996";
        private string course = "Software Engineering";
        private int level = 2;
        private int credits = 120;

        //Processes the accessors for matriculationNumber
        public int MatriculationNumber
        {
            //Returns current value of MatriculationNumber when it is requested
            get
            {
                return matriculationNumber;
            }

            //Sets MatriculationNumber to given value if it is between 40000 and 60000
            set
            {
                if (value < 40000)
                {
                    //Throws exception if matriculation number value is <40000
                    throw new ArgumentException("Matriculation Number is too small.");
                }
                if (value > 60000)
                {
                    //Throws exception if matriculation number value is >60000
                    throw new ArgumentException("Matriculation Number is too large.");
                }
                matriculationNumber = value;
            }
        }

        //Processes the accessors for firstName
        public string FirstName
        {
            //Returns current value of FirstName when it is requested
            get
            {
                return firstName;
            }

            //Sets FirstName to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if first name field is empty
                    throw new ArgumentException("First Name field is empty.");
                }
                firstName = value;
            }
        }

        //Processes the accessors for secondName
        public string SecondName
        {
            //Returns current value of SecondName when it is requested
            get
            {
                return secondName;
            }

            //Sets SecondName to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if second name field is empty
                    throw new ArgumentException("Second Name field is empty.");
                }
                secondName = value;
            }
        }

        //Processes the accessors for dateOfBirth
        public string DateOfBirth
        {
            //Returns current value of DateOfBirth when it is requested
            get
            {
                return dateOfBirth;
            }

            //Sets DateOfBirth to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if date of birth field is empty
                    throw new ArgumentException("Date of birth field is empty.");
                }
                try
                {
                    Convert.ToDateTime(value);
                }
                catch (FormatException)
                {
                    //Throws exception if date of birth field is not in a date format
                    throw new ArgumentException("Date of birth field is not a date.");
                }
                dateOfBirth = value;
            }
        }

        //Processes the accessors for course
        public string Course
        {
            //Returns current value of Course when it is requested
            get
            {
                return course;
            }

            //Sets Course to given value if it is not blank
            set
            {
                if (value == "")
                {
                    //Throws exception if course field is empty
                    throw new ArgumentException("Course field is empty.");
                }
                course = value;
            }
        }

        //Processes the accessors for level
        public int Level
        {
            //returns current value of Level when it is requested
            get
            {
                return level;
            }

            //Sets Level to given value if it is between 1 and 4
            set
            {
                if (value < 1)
                {
                    //Throws exception if level field is < 1 (don't have code for > 4 as it is handled elsewhere)
                    throw new ArgumentException("The level value is too low.");
                }
                level = value;
            }
        }

        //Processes the accessors for credits
        public int Credits
        {
            //Returns current value of Credits when it is requested
            get
            {
                return credits;
            }

            //Sets Credits to given value if it is between 0 and 480
            set
            {
                if (value < 0)
                {
                    //Throws exception if credits value is < 0
                    throw new ArgumentException("The credits value is too low.");
                }
                if (value > 480)
                {
                    //Throws exception if credits value is > 480
                    throw new ArgumentException("The credits value is too high.");
                }
                credits = value;
            }
        }

        //Method to advance the student to their appropriate level if they have enough credits 
        public void advance()
        {
            //Increments level by 1 if there are enough credits and level isn't already 4 (this is why we didn't need a check for > 4 in the set property for level
            if (Level == 1 && Credits >= 120 || Level == 2 && Credits >= 240 || Level == 3 && Credits >= 360)
            {
                Level++;
            }
            else
            {
                //Prevents level from going above 4 as it is the highest level a student can be
                if (Level == 4)
                {
                    throw new ArgumentException("Student is already level 4.");
                }
                //Prevents level from increasing if they don't have enough credits
                else
                {
                    throw new ArgumentException("Student does not have enough credits to advance.");
                }
            }
        }

        
        public void award()
        {
            //Opens the AwardWindow with all award information
            AwardWindow a = new AwardWindow();
            a.lblMatricNumber.Content = (matriculationNumber);
            a.lblName.Content = (secondName + ", " + firstName);
            a.lblCourseLevel.Content = (course + " Year " + level);

            //Sets the award value depending on the number of credits the student has
            string award = "Certificate of Higher Education";
            if (360 <= credits && credits <= 479)
            {
                award = "Degree";
            }
            if (credits >= 480)
            {
                award = "Honours Degree";
            }
            a.lblAwarded.Content = (award);

            a.Show();
        }

    }
}